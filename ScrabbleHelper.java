import java.io.File;
import java.util.ArrayList;

/**
 * Determines all the possible Scrabble words that can be made from a combination of letters
 * 
 * @author Jack Booth
 * @version 3/2/2017
 */
public class ScrabbleHelper
{
	public static void main(String[] args) 
	{
//		Variable declarations
		File file = null;
		String inputString = null;
		Dictionary dictionary = null;
		try
		{
//			Creates the file object if there are at least two arguments.  Otherwise throws an exception.
			if(args.length>1)
			{
				file = new File(args[0]);
			}
			else
			{
				file = new File("");
				throw new IllegalArgumentException();
			}
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("Error: the program expects two command line arguments.  Please rerun program with two arguments in the command line.");
			return;
		}
		
		try
		{
//			checks to see if the string of letters is only letters, otherwise throws an exception
			for(int i = 0; i < args[1].length(); i++)
			{
				if(!Character.isAlphabetic(args[1].charAt(i)))
				{
					throw new IllegalArgumentException();
				}
			}
			inputString = args[1];
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("Error: invalid characters entered. Only letters can be accepted.");
			return;
		}
		
		try
		{
//			Creates a new dictionary object
			dictionary = new Dictionary(file);
		}
		catch(IllegalArgumentException e)
		{
//			Thrown if the file is not found
			System.err.println("Error: invalid file inputted.  Please input a valid file");
			return;
		}
//		creates the Permutations object, and gets all words for that object
		Permutations permBuilder = new Permutations(inputString);
		ArrayList<String> output = permBuilder.getAllWords(dictionary);

//		Prints out all words found
		if(output.size()>0)
		{
			System.out.println("Found " + output.size() + " words:");
			for(int i = 0; i < output.size(); i++)
			{
				System.out.println(output.get(i));
			}
		}
		else
		{
			System.out.println("No words found");
		}
	}
}
