import java.util.ArrayList;

/**
 * Class for generating all permutations or all words from a string of letters.
 * 
 * @author Jack Booth
 * @version 3/2/2017
 */
public class Permutations
{
//	Variable declarations
	private ArrayList<String> allPerms = new ArrayList<String>();
	private ArrayList<String> allDicPerms = new ArrayList<String>();
	private String letters = "";
	
	/**
	 * Constructs a new permutations object based on a string of letters
	 * @param letters - the string to build permutations from
	 * @throws IllegalArgumentException - thrown if a character in the string isn't a letter
	 */
	Permutations(String letters) throws IllegalArgumentException
	{
//		Checks to see if the string only contains letters
		boolean valid = true;
		for(int i = 0; i<letters.length(); i++)
		{
			if(!Character.isLetter(letters.charAt(i)))
				valid = false;
		}
		if(valid)
			this.letters = letters;
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Generates all possible permutations from a string of letters
	 * @return an ArrayList containing all the permutations
	 */
	public ArrayList<String> getAllPermutations()
	{
		permutationBuilder("", letters);
		return allPerms;
	}
	
	/**
	 * Generates all possible words from a string of letters by checking permutations against a dictionary
	 * @param dictionary - the dictionary to check to see if a permutation is a word
	 * @return an ArrayList containing all the words
	 */ 
	public ArrayList<String> getAllWords(Dictionary dictionary)
	{
		dictionaryPermutationBuilder("", letters, dictionary);
		return allDicPerms;
	}
	
	/**
	 * Recursively builds all permutations of a string of letters
	 * @param prefix - the prefix to move to the beginning of the word (to create the permutation)
	 * @param word - the string of letters to build permutations from
	 */
	private void permutationBuilder(String prefix, String word)
	{
//		Base case
		if(word.length() == 0)
		{
			allPerms.add(prefix+word);
		}
		else
		{
//			Builds the permutation recursively
			for(int i = 0; i<word.length(); i++)
			{
				permutationBuilder(prefix + word.charAt(i), word.substring(0, i) + word.substring(i+1, word.length()));
			}
		}
	}
	
	/**
	 * Recursively builds all permutations of a string of letters that are contained in a dictionary
	 * @param prefix - the prefix to move to the beginning of the word (to create the permutation)
	 * @param word - the string of letters to build permutations from
	 * @param dictionary - the dictionary to search and check if a permutation is a word
	 */
	private void dictionaryPermutationBuilder(String prefix, String word, Dictionary dictionary)
	{
//		checks to see if the prefix being added is a prefix in the dictionary
		if(dictionary.isPrefix(prefix))
		{
			if(word.length() == 0)
			{
//				Checks to see if the final permutation is a word, and adds it if it is
				if(!allDicPerms.contains(prefix+word) && dictionary.isWord(prefix+word))
					allDicPerms.add(prefix+word);
			}
			else
			{
//				Builds the permutation recursively
				for(int i = 0; i<word.length(); i++)
				{
					dictionaryPermutationBuilder(prefix + word.charAt(i), word.substring(0, i) + word.substring(i+1, word.length()), dictionary);
				}
			}
		}
	}
	
}
